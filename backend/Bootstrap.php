<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initConfig()
    {
        Zend_Registry::set('APPCONFIG', new Zend_Config($this->getOptions()));
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/backend.ini');
        Zend_Registry::set('FRONTENDCONFIG', $config);
        $mainConfig = Zend_Registry::get('APPCONFIG');
        define('SHOW_UNDER_CONSTRUCTION_PAGES', $mainConfig->isDevelopmentEnv);
    }


    protected function _initRoutes()
    {
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/routes.ini');
        $router->addConfig($config, 'routes');
        Zend_Registry::set('ROUTER' , $front->getRouter());
    }



    protected function _initPlaceholders()
    {
        $appConfig = Zend_Registry::get('APPCONFIG');
        $config = Zend_Registry::get('FRONTENDCONFIG');

        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('HTML5');

        $view->headMeta()
            ->setHttpEquiv('Content-Type', 'text/html; charset=UTF-8')
            ->setHttpEquiv('Content-Language', 'PL')
            ->setName('robots', 'index, follow')
            ->setName('description', $appConfig->websiteName);


        Zend_Paginator::setDefaultScrollingStyle('Sliding');
        Zend_View_Helper_PaginationControl::setDefaultViewPartial(
            'common/_pagination.phtml'
        );

        if (!Zend_Registry::isRegistered('view')) {
            Zend_Registry::set('view', $view);
        }
    }

    /**
     * Inicjalizacja parsera html
     */

    protected  function _initHtmlParser() {
        $path = APPLICATION_PATH. '/../library/simplephpdom/simple_html_dom.php';
        include_once($path);
    }


    /**
     * Ustawienie plików CSS i JS znajdujących się w nagłówku HTML serwisu
     */
    protected function _initScriptsAndStyles()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');

        $view->headScript()
            ->appendFile('/plugins/jQuery/jQuery-2.1.4.min.js')
            ->appendFile('/bootstrap/js/bootstrap.min.js')
            ->appendFile('/plugins/datepicker/bootstrap-datepicker.js')
            ->appendFile('/plugins/slimScroll/jquery.slimscroll.min.js')
            ->appendFile('/plugins/datatables/jquery.dataTables.min.js')
            ->appendFile('/plugins/datatables/dataTables.bootstrap.min.js')
            ->appendFile('/plugins/fastclick/fastclick.min.js')
            ->appendFile('/plugins/iCheck/icheck.min.js')
            ->appendFile('/dist/js/app.min.js')
            ->appendFile('/js/plupload/plupload.full.min.js')
            ->appendFile('/js/highcharts.js')
            ->appendFile('/js/exporting.js')
            ->appendFile('/js/cms.js');

        $view->headLink()
            ->appendStylesheet('/bootstrap/css/bootstrap.min.css')
            ->appendStylesheet('/plugins/datepicker/datepicker3.css')
            ->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css')
            ->appendStylesheet('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')
            ->appendStylesheet('/plugins/datatables/dataTables.bootstrap.css')
            ->appendStylesheet('/dist/css/AdminLTE.min.css')
            ->appendStylesheet('/plugins/iCheck/square/blue.css')
            ->appendStylesheet('/dist/css/skins/_all-skins.min.css')
            ->appendStylesheet('/css/main.css');


    }

    /**
     * Inicjalizacja obiektu dostępu do bazy danych
     */
    protected function _initDb()
    {
        $config = Zend_Registry::get('APPCONFIG');
        $db = Zend_Db::factory($config->resources->db->adapter, $config->resources->db->params->toArray());
        $db->setFetchMode(Zend_Db::FETCH_ASSOC);
        Zend_Db_Table::setDefaultAdapter($db);
        Zend_Registry::set('DB', $db);


    }

    /**
     * Laduje ustawienia ZFDebug i wyswietla
     * @return void
     */
    protected function _initZFDebug()
    {
        $config = Zend_Registry::get('APPCONFIG');

        if($config->zfdebug) {
            $autoloader = Zend_Loader_Autoloader::getInstance();
            $autoloader->registerNamespace('ZFDebug');

            $options = $config->zfdebug->toArray();

            $options['plugins']['Database']['adapter'] = Zend_Registry::get('DB');

            $debug = new ZFDebug_Controller_Plugin_Debug($options);

            $this->bootstrap('frontController');
            $this->getResource('frontController')->registerPlugin($debug);
        }
    }
}

