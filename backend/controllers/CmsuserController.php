<?php

class CmsuserController extends ETL_FrontendController
{
    private $salt = '';

    public function init()
    {
        /* Initialize action controller here */
        parent::init();
        $this->isCmsLogged();
        $this->view->menu = 'cmsusers';


        $config = $this->view->config = Zend_Registry::get('APPCONFIG');
        $this->salt = $config->salt;
    }

    public function indexAction()
    {
        // action body

        $cmsUsersModel = new Model_DbTable_CmsUsers();
        $users = $cmsUsersModel->fetchAll();
        $this->view->list = $users;
    }



    public  function newAction() {

        $form = new ETL_CmsUserForm(null, $this->_user);
        $this -> _helper -> viewRenderer('edit');
        $this->view->header = 'Nowy  użytkownik';

        $userModel = new Model_DbTable_CmsUsers();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $dataForm = $form->getValues();
                $dataForm['usr_createtime'] = date('Y-m-d');
                $password =ETL_Common::generateRandomPassword();
                $dataForm['usr_password']=sha1($password.$this->salt);

                $message ="Witaj. Właśnie utworzono Ci konto w etl <br>Twój login to:".$dataForm['usr_email']."<br>Twoje hasło to: ".$password."<br>Zaloguj się : <a href='http://".$_SERVER['SERVER_NAME']."'>Tutaj</a>";
                ETL_Common::sendMail($dataForm['usr_email'],"Utworzono konto",$message );

                $id = $userModel->insert($dataForm);
                return $this-> _helper -> redirector ('index', 'cmsuser', 'default');

            } else {
                $this->view->validationError=1;
            }

        }

        $this->view->form = $form;
    }

    public function blockAction() {


        $userModel = new Model_DbTable_CmsUsers();

        $id = $this->getParam('id',0);
        if ($id == 0) throw new Zend_Exception('BŁĄD',404);

        $obj = $userModel->find($id)->current();
        if (!$obj) {
            throw new Zend_Controller_Action_Exception('Błąd - brak elementu',404);
        }


        if ($this->_user->usr_id != $id) {
            if ($obj->usr_active == 0) {
                $obj->usr_active = 1;
            } else {
                $obj->usr_active = 0;
            }
        }

        $obj->save();
        return $this-> _helper -> redirector ('index', 'cmsuser', 'default');
    }


    public function editdataAction(){
        $this->view->submenu = 'changeuser';
        $form = new ETL_CmsUserForm($this->_user->usr_id, $this->_user);
        $this -> _helper -> viewRenderer('edit');
        $this->view->header = 'Zmień dane użytkownika';

        $userModel = new Model_DbTable_CmsUsers();

        $obj = $userModel->find($this->_user->usr_id)->current();


        $form->populate($obj->toArray());

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $dataForm = $form->getValues();

                $obj->setFromArray($dataForm);
                $obj->save();
                $this->view->saved=1;

            } else {
                $this->view->validationError=1;
            }

        }

        $this->view->form = $form;
    }

    public function editpasswordAction() {
        $this->view->menu = 'changeuser';
        $form = new ETL_NewPasswordForm();
        $this -> _helper -> viewRenderer('edit');
        $this->view->header = 'Zmień hasło';

        $userModel = new Model_DbTable_CmsUsers();
        $obj = $userModel->find($this->_user->usr_id)->current();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $dataForm = $form->getValues();
                $obj->usr_password = sha1($dataForm['usr_password'] . $this->salt);
                $obj->save();
                $this->view->saved=1;
            } else {
                $this->view->validationError=1;
            }

        }

        $this->view->form = $form;
    }




}

