<?php

class OptionsController extends ETL_FrontendController
{

    public function init()
    {
        /* Initialize action controller here */
        parent::init();
        $this->isCmsLogged();
        $this->view->menu = 'options';
    }

    public function indexAction()
    {
        // action body

        $options = new Model_DbTable_Options();
        $optionsList = $options->fetchAll();
        $this->view->list = $optionsList;
    }

    public  function editAction()
    {
        $form = new ETL_OptionsForm();
        $options = new Model_DbTable_Options();

        $id = $this->getParam('id',0);
        if ($id == 0) throw new Zend_Exception('BŁĄD',404);

        $obj = $options->find($id)->current();
        if (!$obj) {
            throw new Zend_Controller_Action_Exception('Błąd - brak elementu',404);
        }

        $optionData = $obj->toArray();
        $form->populate($optionData);

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $dataForm = $form->getValues();

                $obj->setFromArray($dataForm);
                $obj->save();
                $this->view->saved=1;

            } else {
                $this->view->validationError=1;
            }

        }

        $this->view->form = $form;
    }


}

