<?php

class IndexController extends ETL_FrontendController
{

    protected $sessionData = null;

    public function init()
    {
        parent::init();
        $this->isCmsLogged();
        $this->view->menu = 'index';

        $this->sessionData = new Zend_Session_Namespace('etl');

    }

    /**
     * Akcja po zalogowaniu
     */
    public function indexAction()
    {

    }

    public function truncateAction() {
        $opinionModel = new Model_DbTable_Opinions();
        $opinionModel->truncateTable();
    }

    /**
     * Pełny proces etl
     */
    public function etlAction(){
        $id = (int) $this->getParam('productid',0);
        if ($id == 0)   return $this->_helper->json(array('status'=>'ERROR'));

        $res = $this->extract($id,true);
        $this->transfromCeneo();
        $this->transformMorele(true);
        $this->sync();
    }


    /**
     * Proces transformacji danych
     */

    public function transformAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->transfromCeneo();
        $this->transformMorele();
    }


    /**
     * Proces ładowania danych
     */

    public function syncAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->sync();
    }

    /**
     * Akcja podejrzenia danych tymczasowych
     */

    public function resultAction() {
        $this->view->sess = $this->sessionData;
    }


    public function extractAction()
    {

        $id = (int) $this->getParam('productid',0);
        if ($id == 0)   return $this->_helper->json(array('status'=>'ERROR'));

        $this->extract($id);

    }

    /**
     * @param $id
     * @param bool $full
     * @return bool
     * Pobiera dane z serwisu morele.net i ceneo i zapisuje wynik do sesji. Pobiera równiez dane o produkcie
     */

    protected function extract($id, $full = false){
        $idCeneo = $id;
        $this->sessionData->ceneoID = $idCeneo;

        $ceneoData = false;
        $ceneoData =  $this->getFromCeneo($idCeneo);

        $productData =  $this->sessionData->product;
        $this->sessionData->moreleRaw =  $this->getMoreleComments($productData['plain']);

        $prd = $this->sessionData->product;
        if (!$full) {
            return $this->_helper->json(array('status'=>'OK', 'pages' => ($this->sessionData->ceneoPages +1 ), 'product' => $prd ));
        } else {
            return true;
        }
    }

    /**
     * @param bool $full
     * @return mixed
     * zwraca jako json ilosc opinii dla obu serwisów
     */
    protected function transformMorele($full = false) {

        $this->sessionData->morele = $this->parseMoreleComments($this->sessionData->moreleRaw);
        $commentsCeneo = count($this->sessionData->ceneo);
        $commentsMorele = count($this->sessionData->morele);
        if (!$full) {
            return $this->_helper->json(array('status'=>'OK', 'ceneo' => $commentsCeneo, 'morele'=> $commentsMorele));
        }

    }


    /**
     * @param $idCeneo
     * @return bool
     * Pobiera dane produktu z ceneo
     */
    protected function getFromCeneo($idCeneo)
    {
        $html = new simple_html_dom();
        $rawCeneo = my_curl('http://www.ceneo.pl/' . $idCeneo, array());
        $html->load($rawCeneo);

        if (empty($rawCeneo))  {
            return false;
        }

        $productData = array();
        $type = $html->find('.breadcrumbs .breadcrumb a span', -1);
        if (!empty($type)) {
            $productData['type'] = $type->innertext;
        }

        $name = $html->find('h1.product-name', 0);
        if (!empty($name)) {
            $name = $name->innertext;
            $firstSpace = strpos($name, ' ');
            $manufacturer = trim(substr($name, 0, $firstSpace));
            $model = trim(substr($name, $firstSpace));

            $productData['manufacturer'] = $manufacturer;
            $productData['model'] = $model;
            $productData['plain'] =  $name;
        }


        $specs = $html->find('#productTechSpecs .specs-group');
        $specsPlain = '';
        foreach ($specs as $s) {
            $specsPlain .= preg_replace('/\s\s+/', ' ', $s->innertext);
        }
        $productData['specs'] = $specsPlain;
        $this->sessionData->product = $productData;

        //getProductCommentCount
        $count = $html->find('.product-meta .product-reviews-link span', -1);
        if (!empty($count)) {
            $count = $count->innertext;
            $pages = floor($count / 10) + 1;
            $this->sessionData->ceneoPages = $pages;
        }

         return $this->getCeneoComments();


    }

    /**
     * @return bool
     * Pobiera opinie z ceneo, zapisuje w sesji wynik
     */

    protected function getCeneoComments()
    {
        $ceneoData = array();
        for ($i = 1; $i <= $this->sessionData->ceneoPages; $i++) {
            $ceneoData[] = $this->getCommentsForPage($i);
        }

        $this->sessionData->ceneoRaw = $ceneoData;

        return true;
    }

    /**
     * @param $pageNo
     * @return bool|mixed
     * Wykonanie curl do serwisu ceneo w celu pobrania html dla konkretnej strony z opiniamii
     */

    protected function getCommentsForPage($pageNo) {
        $urlRawData = my_curl('http://www.ceneo.pl/' . $this->sessionData->ceneoID . '/opinie-' . $pageNo, array(), 4);
        return $urlRawData;
    }

    /**
     * @return bool
     * Przetwarza pobrane dane z serwisu ceno do postaci tablicowej
     */

    protected function transfromCeneo() {
        $data = $this->sessionData->ceneoRaw;
        $j = 0;
        $commentData = array();

        foreach ($data as $html) {
            $d = new simple_html_dom();
            $d->load($html);
            $comments = $d->find('.product-reviews .product-review');
            foreach ($comments as $cnt) {

                $text = $cnt->find('.product-review-body',0);
                if (!empty($text)) { $commentData[$j]['text'] = trim($text->innertext); }

                $pros = $cnt->find('.pros-cell ul', 0);
                if (!empty($pros)) {
                    $plain  = str_replace(array('<li>','</li>'), array('','<br />'),strip_tags($pros->innertext, '<li>'));
                    $plain = rtrim(trim($plain),'<br />');

                    $commentData[$j]['pros'] = preg_replace('/\s\s+/', ' ', $plain);
                }

                $cons = $cnt->find('.cons-cell ul', 0);
                if (!empty($cons)) {
                    $plain  = str_replace(array('<li>','</li>'), array('','<br />'),strip_tags($cons->innertext, '<li>'));
                    $plain = rtrim(trim($plain),'<br />');
                    $commentData[$j]['cons'] = preg_replace('/\s\s+/', ' ', $plain);
                }

                $date = $cnt->find('.review-time time', 0);
                if (!empty($date)) $commentData[$j]['date'] = trim($date->datetime);

                $author = $cnt->find('.product-reviewer', 0);
                if (!empty($author)) $commentData[$j]['author'] = trim($author->innertext);

                $rate = $cnt->find('.review-score-count', 0);
                if (!empty($rate)) {
                    $ratearr = explode('/',$rate->innertext);
                    $ratetext = array_shift($ratearr);
                    $rate = str_replace(',','.',$ratetext);
                    $commentData[$j]['rate'] = $rate;
                }

                $possesion = $cnt->find('.product-review-pz', 0);
                if (!empty($possesion)) {
                    $commentData[$j]['possesion'] = 'Osoba kupiła produkt'; }
                else {
                    $commentData[$j]['possesion'] = 'Osoba nie kupiła produktu';
                }

                $opnRateData = $cnt->find('.product-review-usefulness-stats span');
                if (!empty($opnRateData) && count($opnRateData) > 2) {
                    $commentData[$j]['rateCount'] = $opnRateData[2]->innertext;
                    $commentData[$j]['ratePercent'] = $opnRateData[0]->innertext;
                } else {
                    $commentData[$j]['rateCount'] = 0;
                    $commentData[$j]['ratePercent'] = 0;
                }

                $recommend = $cnt->find('.product-review-summary .product-recommended', 0);
                if (!empty($recommend)) {
                    $commentData[$j]['recommend'] =   $recommend->innertext;
                }

                $idInput = $cnt->find('.report-product-review-abuse', 0);
                if (!empty($idInput)) $commentData[$j]['ceneoID'] = $idInput->getAttribute('data-review-id');
                $j++;

                unset($text, $pros,$cons,$date, $author, $rate, $possesion, $opnRateData, $recommend, $idInput);
            }
            unset($d);
        }

        $this->sessionData->ceneo = $commentData;

        return true;
    }

    /**
     * @param $prdName
     * @return bool|mixed
     * Pobranie kodu html z opiniamii dla produktu z serwisu morele.net szukając po jego nazwie
     */

    protected function getMoreleComments($prdName)
    {
        $morele = new simple_html_dom();
        $moreleRawData = my_curl('http://www.morele.net/search/netsprint/0/1/0/?q=' . urlencode($prdName), array(), 8);

        $morele->load($moreleRawData);
        // get from morele


        $moreleLink = $morele->find('#content ul a', 0); // find if search list
        $morelePrdId = $morele->find('.id-price .product_id', 0);


        if ($moreleLink != null) {
            $moreleLink = $moreleLink->href;
            $morelePage = new simple_html_dom();

            $morelePageData = my_curl($moreleLink, array(), 6);
            $morelePage->load($morelePageData);

            $prdId = $morelePage->find('.id-price .product_id', 0);
            if ($prdId != null) {
                $this->sessionData->moreleID = $prdId->innertext;
                return $this->getMoreleData($prdId->innertext);
            }

        } elseif ($morelePrdId != null) {
            $this->sessionData->moreleID = $morelePrdId->innertext;
            return $this->getMoreleData($morelePrdId->innertext);

        }
        return false;
    }

    /**
     * @param $id
     * @return bool|mixed
     * Wykonanie żądania curl w celu pobrania opinii dla konkretnego produktu z serwisu morele.net
     */

    protected function getMoreleData($id)
    {
        $moreleComments = new simple_html_dom();
        $moreleCommentsData = my_curl('http://www.morele.net/catalog/ajaxGetReviews/' . $id, array(),4);
//        $moreleComments->load($moreleCommentsData);

        return $moreleCommentsData;
    }

    /**
     * @param $commentsRaw
     * @return array
     * Przetwarza pobrane dane z serwisu morele do postaci tablicowej
     */
    protected function parseMoreleComments($commentsRaw)
    {
        $comments = new simple_html_dom();
        $comments->load($commentsRaw);

        $lists = $comments->find('ul');
        $commentData = array();
        $j = -1;

        foreach ($lists as $k => $ul) {
            $mod = $k % 4;
            switch ($mod) {
                case 0: {
                    $j++;
                    $commentData[$j] = array();
                    $text = $ul->find('.rcomment', 0);
                    if (!empty($text)) $commentData[$j]['text'] = $text->innertext;

                    $pros = $ul->find('.good p', 0);
                    if (!empty($pros)) $commentData[$j]['pros'] = $pros->innertext;

                    $cons = $ul->find('.bad p', 0);
                    if (!empty($pros)) $commentData[$j]['cons'] = $cons->innertext;
                    break;
                }

                case 1: {
                    $date = $ul->find('.date', 0);
                    if (!empty($date)) $commentData[$j]['date'] = $date->innertext;

                    $author = $ul->find('.author strong', 0);
                    if (!empty($author)) $commentData[$j]['author'] = $author->innertext;

                    $rate = $ul->find('.rate img', 0);
                    if (!empty($rate)) {
                        $ratearr = explode('/', $rate->src);
                        $filename = array_pop($ratearr);
                        $rate = floatval(str_replace(array('ratestar_', '.gif'), array('', ''), $filename)) / 2;
                        $commentData[$j]['rate'] = $rate;
                    }

                    $possesion = $ul->find('.have-product', 0);
                    if (!empty($possesion)) $commentData[$j]['possesion'] = ''; //$possesion->innertext;

                    $opnRateCount = $ul->find('.rate-count strong', 0);
                    if (!empty($opnRateCount)) $commentData[$j]['rateCount'] = $opnRateCount->innertext;

                    $opnRatePercent = $ul->find('.rate-count strong', 1);
                    if (!empty($opnRatePercent)) $commentData[$j]['ratePercent'] = $opnRatePercent->innertext;

                    $idInput = $ul->find('.submit form input', 0);
                    if (!empty($idInput)) $commentData[$j]['moreleID'] = $idInput->value;

                    $commentData[$j]['recommend'] =   '';

                    break;
                }
                default: {

                }
            }


        }

        unset($comments);

        return $commentData;


    }


    /**
     * @return mixed
     * Zapisuje przetworzone dane
     */

    protected function sync() {
        $ceneoID =  $this->sessionData->ceneoID;
        $moreleId = $this->sessionData->moreleID;
        $productData = $this->sessionData->product;
        $productData['morele_id'] = $moreleId;
        $ceneoComments = $this->sessionData->ceneo;


        $moreleComments = $this->sessionData->morele;

        $productModel = new Model_DbTable_Products();
        $productModel->syncProduct($ceneoID,$productData);

        $opinionModel = new Model_DbTable_Opinions();
        $ceneoOpinions = $this->processOpinions($ceneoComments,$ceneoID);
        $moreleOpinions = $this->processOpinions($moreleComments,$ceneoID);

        $ceneoResult = $opinionModel->processCeneo($ceneoOpinions);
        $moreleResult = $opinionModel->processMorele($moreleOpinions);


        $result = array_merge($ceneoResult,$moreleResult);
        $result['url'] = '/product/product/id/'.$ceneoID;
        $result['status'] = 'OK';

        Zend_Session::namespaceUnset('etl');

        return $this->_helper->json($result);
    }

    /**
     * @param $data
     * @param $ceneoID
     * @return array
     * Zwraca tablice, ktorej kluczem jest wartosc ID opinii, dodaje pole z ID produktu z ceneo
     */

    protected function processOpinions($data,$ceneoID) {
        $processed = array();
        foreach ($data as $k => $d) {
            $processed[$k] = $d;
            $processed[$k]['prd_id'] = $ceneoID;
        }

        return $processed;
    }

}

