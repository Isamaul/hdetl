<?php

class AuthController extends ETL_FrontendController
{
    private $salt=  '';

    public function init()
    {
        /* Initialize action controller here */
        parent::init();

        $layout = $this->_helper->layout();
        $layout->setLayout('login');

        $config = $this->view->config = Zend_Registry::get('APPCONFIG');
        $this->salt = $config->salt;
    }

    public function indexAction()
    {
        // action body
        $this -> view -> form = new ETL_CmsLoginForm();
        $url = $this -> view -> url(array('action' => 'login'));
        $this ->view-> form ->setAction($url);
    }

    public function loginAction()
    {
        // action body
        $this -> _helper -> viewRenderer('index');
        $form = new ETL_CmsLoginForm();
        $url = $this -> view -> url(array('action' => 'login'));
        $form ->setAction($url);
        if ($this->getRequest()->isPost()){


            if ($form -> isValid($this->getRequest()->getPost())) {

                // sprawdzamy dane, czy to jest uzytkownik
                $adapter = new Zend_Auth_Adapter_DbTable(
                    null,'cms_users', 'usr_email', 'usr_password',null); // 'SHA1(CONCAT(?,salt))' za ostani null

                $adapter -> setIdentity($form->getValue('usr_email'));
                $adapter -> setCredential(sha1($form -> getValue('usr_password').$this->salt));
                $auth = Zend_Auth::getInstance();

                $result = $auth->authenticate($adapter);

                // stworzony obiekt, sprawdzamy czy posiadamy uzytkownika w bazie, jezeli tak przekierujmy na jego strone
                if ($result->isValid()) {
                    $name = $result->getIdentity();
                    $storage = $auth->getStorage();
                    $user =$adapter->getResultRowObject();
                    if ($user->usr_active == 0) { throw new Zend_Exception(404,'Konto zablokowane');}
                    unset($user->usr_password);
                    $storage->write($user);
                    return $this-> _helper -> redirector ('index', 'index', 'default');
                } else
                {
                    $form->usr_password->addError('Błędny login lub hasło');
                }


            }

        }


        $this -> view -> form = $form;
    }

    public function logoutAction()
    {
        // action body

        $auth = Zend_Auth::getInstance();
        $auth -> clearIdentity();
        return $this-> _helper -> redirector ('index', 'index', 'default');
    }


    public function lostpasswordAction() {

        //@TODO

        if (Zend_Auth::getInstance()->hasIdentity()) return  $this-> _helper -> redirector ('index', 'index', 'default');
        $form = new ETL_LostPasswordForm();
        $this->view ->form = $form;
        $url = $this -> view -> url(array('action' => 'lostpassword'));
        $this ->view-> form ->setAction($url);

        if ($this -> getRequest() -> isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $data = $form->getValues();

                $user = new Model_DbTable_CmsUsers();
                $user_data=$user->getByEmail($data['email']);

                if (!empty($user_data)) {
                    $hash = sha1(($user_data['usr_email'].time()));
                    $obj = $user->find($user_data['usr_id'])->current();
                    $obj -> usr_hash = $hash;
                    $obj ->save();
                    $login =$user_data['usr_name'].' '.$user_data['usr_lastname'];
                    $link = "http://".$_SERVER['SERVER_NAME'].'/auth/new-password/id/'.$hash;
                    $text =
                        "Witaj  $login
Właśnie zostało zresetowane hasło w naszym serwisie. Aby je zmienić wejdź na adres: $link";
                    ETL_Common::sendMail($user_data['usr_email'],"Utracone hasło",$text);

                }
                return $this-> _helper -> redirector ('checkinbox', 'auth', 'default');

            }

        }

    }

    public function checkinboxAction()
    {

    }

    public function passwordchangedAction()
    {

    }


    public function newPasswordAction() {
        $id = $this -> getRequest() -> getParam('id');
        if (empty($id))  throw new Zend_Exception('Wystąpił błąd na stronie. Być może adres jest nieprawidłowy?');
        $form = new ETL_NewPasswordForm();
        $this->view->form = $form;


        $user = new Model_DbTable_CmsUsers();
        $user_data= $user->getByHash($id);

        if ( empty($user_data)) throw new Zend_Exception('Błąd - nie odnaleziono rekordów spełniających określone wymagania');

        if ($this -> getRequest() -> isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $data = $form->getValues();
                $obj = $user->find($user_data->usr_id)->current();
                $obj->usr_password = sha1($data['usr_password'] . $this->salt);
                $obj->usr_hash = null;
                $obj->save();

                return $this-> _helper -> redirector ('passwordchanged', 'auth', 'default');


            }

        }

    }





}





