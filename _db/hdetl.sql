-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07 Sty 2016, 20:54
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hdetl`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_users`
--

CREATE TABLE IF NOT EXISTS `cms_users` (
  `usr_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_email` varchar(245) NOT NULL,
  `usr_password` varchar(245) NOT NULL,
  `usr_name` varchar(245) DEFAULT NULL,
  `usr_lastname` varchar(245) DEFAULT NULL,
  `usr_type` enum('ROOT','WORKER') DEFAULT 'WORKER',
  `usr_createtime` datetime DEFAULT NULL,
  `usr_active` tinyint(1) DEFAULT '1',
  `usr_hash` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`usr_id`),
  UNIQUE KEY `usr_email_UNIQUE` (`usr_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `cms_users`
--

INSERT INTO `cms_users` (`usr_id`, `usr_email`, `usr_password`, `usr_name`, `usr_lastname`, `usr_type`, `usr_createtime`, `usr_active`, `usr_hash`) VALUES
(1, 'grzegorz.wziatek@gmail.com', '905138fede542187eefabe9a351353adb706a353', 'Grzegorz', 'Wziątek', 'ROOT', '2015-12-06 00:00:00', 1, NULL),
(2, 'admin@wziatek.net', '905138fede542187eefabe9a351353adb706a353', 'test', 'test', 'ROOT', '2016-01-06 00:00:00', 1, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `opinions`
--

CREATE TABLE IF NOT EXISTS `opinions` (
  `opn_id` int(11) NOT NULL AUTO_INCREMENT,
  `text` longtext,
  `date` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT '',
  `possesion` varchar(255) DEFAULT '',
  `rate` decimal(4,2) DEFAULT NULL,
  `rateCount` int(11) DEFAULT '0',
  `ratePercent` decimal(4,2) DEFAULT '0.00',
  `recommend` varchar(255) DEFAULT '',
  `pros` longtext,
  `cons` longtext,
  `ceneoID` int(11) DEFAULT NULL,
  `moreleID` int(11) DEFAULT NULL,
  `prd_id` int(11) NOT NULL,
  PRIMARY KEY (`opn_id`),
  KEY `key` (`ceneoID`,`moreleID`),
  KEY `opn_idx` (`prd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Zrzut danych tabeli `opinions`
--

INSERT INTO `opinions` (`opn_id`, `text`, `date`, `author`, `possesion`, `rate`, `rateCount`, `ratePercent`, `recommend`, `pros`, `cons`, `ceneoID`, `moreleID`, `prd_id`) VALUES
(1, 'w trakcie oceny', '2015-12-03 09:19:21', 'Krzysztof', 'Osoba kupiła produkt', '4.00', 2, '50.00', 'Polecam', NULL, NULL, 3451675, NULL, 37384121),
(2, 'Doskonałe narzędzie do pracy i do zabawy. Bardzo dobra jakość wykonania. Rewelacyjna współpraca software i hardware. Bardzo dobry czas pracy na baterii.', '2015-05-27 21:38:08', 'schilack', 'Osoba nie kupiła produktu', '5.00', 12, '92.00', 'Polecam', 'integracja z innymi urządzeniami apple<br /> jakość<br /> wsp&#243;łpraca z systemem operacyjnym<br /> wydajność<br /> wygląd', NULL, 3164359, NULL, 37384121),
(3, 'Kto zna firmę ten wie co kupuje, a kto nie zna powinien się zainteresować. Świetny komputer do prac biurowych, lekkiej rozrywki i do prostej obróbki photo i video. Długa praca na baterii to jego bardzo duża zaleta. System fantastyczny.', '2015-09-22 13:29:15', 'Marcin', 'Osoba nie kupiła produktu', '5.00', 3, '99.99', 'Polecam', NULL, NULL, 3333995, NULL, 37384121),
(4, 'Genialny laptop, najlepszy w swojej klasie, miałem wcześniej ultrabooka asus ux21e i stwierdzam, że apple mimo wszystko jest lepsze ze względu na świetną integrację sprzętu z softwarem, oraz bezkompromisowość w konfiguracji sprzętowej.', '2015-07-19 12:55:49', 'GamesZentral', 'Osoba nie kupiła produktu', '5.00', 4, '75.00', 'Polecam', 'integracja z innymi urządzeniami apple<br /> jakość<br /> lekkość<br /> wsp&#243;łpraca z systemem operacyjnym<br /> wydajność<br /> wygląd', NULL, 3241356, NULL, 37384121),
(5, 'ok', '2016-01-06 11:38:44', 'Miko', 'Osoba nie kupiła produktu', '5.00', 0, '0.00', 'Polecam', NULL, NULL, 3516518, NULL, 37384121),
(6, 'ok', '2015-12-17 19:22:57', 'geo', 'Osoba nie kupiła produktu', '5.00', 0, '0.00', 'Polecam', NULL, NULL, 3481938, NULL, 37384121),
(7, 'wszystko ok', '2015-12-17 17:22:34', 'luky', 'Osoba nie kupiła produktu', '5.00', 0, '0.00', 'Polecam', NULL, NULL, 3481679, NULL, 37384121),
(8, 'Kupiłam produkt który ma światową renomę i moja ocena trochę nie m sensu. Wiedziałam co kupuję i nie jestem zaskoczona.', '2015-11-28 16:09:50', 'Dorota', '', '5.00', 1, '99.99', '', '', '', NULL, 435193, 37384121),
(9, 'Świetny laptop jestem bardzo zadowolony. Cichutki, bateria trzyma bardzo bardzo długo, sprawny i szybki.', '2015-10-07 14:46:40', '', '', '5.00', 3, '66.67', '', '', '', NULL, 423703, 37384121),
(10, 'Sprzęt sprawuje się idalnie do tego w jakim celu został stworzony. Cichy, lekki i piękny. System niezawodny i induicyjny. Do biura, pracy czy na studia idealny :)', '2015-06-06 20:15:58', 'Weronika', '', '5.00', 15, '60.00', '', 'system<br/> bateria - 12h<br/> nie słychać, że działa<br/> nie grzeje sie<br/> gabaryty<br/> waga<br/> wygląd<br/> touchpad<br/> klawiatura', 'dla niektórych cena, ale chcąc kupić ultrabooka na windowsie z takimi parametrami, gabarytami i baterią zapłacimy podobnie.', NULL, 394525, 37384121);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `opt_id` int(11) NOT NULL AUTO_INCREMENT,
  `opt_name` varchar(245) NOT NULL DEFAULT '',
  `opt_value` text,
  `opt_namespace` varchar(245) NOT NULL,
  `opt_image` tinyint(1) NOT NULL DEFAULT '0',
  `opt_key` varchar(245) NOT NULL,
  PRIMARY KEY (`opt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Zrzut danych tabeli `options`
--

INSERT INTO `options` (`opt_id`, `opt_name`, `opt_value`, `opt_namespace`, `opt_image`, `opt_key`) VALUES
(1, 'Tytuł Strony', 'HD ETL', 'page', 0, 'page_title'),
(3, 'Użytkownik e-mail', 'fa@wziatek.net', 'mail', 0, 'mail_username'),
(4, 'Hasło e-mail', 'fa;;;test;;;email', 'mail', 0, 'mail_password'),
(5, 'Serwer smtp', 'mail.wziatek.net', 'mail', 0, 'mail_smtp'),
(6, 'Słowa Kluczowe', 'ETL', 'page', 0, 'page_keywords'),
(7, 'Opis strony', 'Opis strony', 'page', 0, 'page_description');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `prd_id` int(11) NOT NULL,
  `morele_id` int(11) DEFAULT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `plain` varchar(255) DEFAULT NULL,
  `specs` longtext,
  PRIMARY KEY (`prd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `products`
--

INSERT INTO `products` (`prd_id`, `morele_id`, `manufacturer`, `model`, `type`, `plain`, `specs`) VALUES
(37384121, 723187, 'Apple', 'MacBook Air 13 (MJVE2ZE/A)', 'Ultrabooki', 'Apple MacBook Air 13 (MJVE2ZE/A)', ' <header class="section-header"> <h3>Podstawowe informacje</h3> </header> <table> <tbody> <tr> <th> Producent </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/p:Apple.htm">Apple</a> </li> </ul> </td> </tr> <tr> <th> Kolor </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/Kolor:Szary_i_srebrny.htm">Szary i srebrny</a> </li> </ul> </td> </tr> <tr> <th> Częstotliwość procesora </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/Czestotliwosc_procesora:1600_MHz.htm">1600 MHz</a> </li> </ul> </td> </tr> <tr> <th> Rodzina procesora </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/Rodzina_procesora:Intel_Core_i5.htm">Intel Core i5</a> </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Fizyczne</h3> </header> <table> <tbody> <tr> <th> Waga </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki;018P18F1240993T1240993.htm">1,35kg</a> </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Ekran</h3> </header> <table> <tbody> <tr> <th> Przekątna ekranu </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/Przekatna_ekranu:13_3_cala.htm">13,3 cala</a> </li> </ul> </td> </tr> <tr> <th> Ekran dotykowy </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/Ekran_dotykowy:Nie_posiada_ekranu_dotykowego.htm">Nie posiada ekranu dotykowego</a> </li> </ul> </td> </tr> <tr> <th> Producent karty graficznej </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/Producent_karty_graficznej:Intel.htm">Intel</a> </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Specyfikacja</h3> </header> <table> <tbody> <tr> <th> Pamięć RAM </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/Pamiec_RAM:4GB.htm">4GB</a> </li> </ul> </td> </tr> <tr> <th> Dysk twardy </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/Dysk_twardy:128GB_SSD.htm">128GB SSD</a> </li> </ul> </td> </tr> <tr> <th> Napęd optyczny </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/Naped_optyczny:Nie_posiada_wbudowanego_napedu.htm">Nie posiada wbudowanego napędu</a> </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Oprogramowanie</h3> </header> <table> <tbody> <tr> <th> System operacyjny </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/System_operacyjny:Mac_OS_X.htm">Mac OS X</a> </li> </ul> </td> </tr> </tbody> </table>  <table> <tbody> <tr> <th> Ilość rdzeni procesora </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Ultrabooki/Ilosc_rdzeni_procesora:2_rdzenie_(DualCore).htm">2 rdzenie (DualCore)</a> </li> </ul> </td> </tr> <tr> <th> Kod producenta </th> <td> <ul> <li class="attr-value"> MJVE2ZE/A </li> </ul> </td> </tr> </tbody> </table> '),
(37516287, 789028, 'Huawei', 'P8 szary/srebrny', 'Smartfony', 'Huawei P8 szary/srebrny', ' <header class="section-header"> <h3>Dane podstawowe</h3> </header> <table> <tbody> <tr> <th> Producent </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/producenci/huawei/Smartfony">Huawei</a> </li> </ul> </td> </tr> <tr> <th> System operacyjny </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/System_operacyjny:Android.htm">Android</a> </li> </ul> </td> </tr> <tr> <th> Typ budowy </th> <td> <ul> <li class="attr-value"> Dotykowy </li> </ul> </td> </tr> <tr> <th> Pamięć wewnętrzna </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Pamiec_wewnetrzna:16_GB.htm">16 GB</a> </li> </ul> </td> </tr> <tr> <th> Pamięć RAM </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Pamiec_RAM:3_GB.htm">3 GB</a> </li> </ul> </td> </tr> <tr> <th> Dual SIM </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Dual_SIM:Nie.htm">Nie</a> </li> </ul> </td> </tr> <tr> <th> Rodzaj karty pamięci </th> <td> <ul> <li class="attr-value"> microSDXC </li> <li class="attr-value"> microSDHC </li> <li class="attr-value"> microSD </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Ekran</h3> </header> <table> <tbody> <tr> <th> Przekątna ekranu </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Przekatna_ekranu:5_2_cala.htm">5,2 cala</a> </li> </ul> </td> </tr> <tr> <th> Technologia wyświetlacza </th> <td> <ul> <li class="attr-value"> IPS TFT </li> </ul> </td> </tr> <tr> <th> Rozdzielczość ekranu </th> <td> <ul> <li class="attr-value"> 1080x1920 </li> </ul> </td> </tr> <tr> <th> Ilość kolor&#243;w wyświetlacza </th> <td> <ul> <li class="attr-value"> 16M </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Aparat</h3> </header> <table> <tbody> <tr> <th> Wbudowany aparat cyfrowy </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Wbudowany_aparat_cyfrowy:13_Mpix.htm">13 Mpix</a> </li> </ul> </td> </tr> <tr> <th> Rozdzielczość aparatu </th> <td> <ul> <li class="attr-value"> 4160x3120 </li> </ul> </td> </tr> <tr> <th> Lampa błyskowa </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Przedni aparat </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Video </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Formaty video </th> <td> <ul> <li class="attr-value"> H.263 </li> <li class="attr-value"> H.264 </li> <li class="attr-value"> MPEG4 </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Łączność</h3> </header> <table> <tbody> <tr> <th> Moduł WiFi </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Hotspot WiFi </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Technologia LTE </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Technologia_LTE:Tak.htm">Tak</a> </li> </ul> </td> </tr> <tr> <th> NFC </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> GPS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> A-GPS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Bluetooth </th> <td> <ul> <li class="attr-value"> v4.1 </li> </ul> </td> </tr> <tr> <th> DLNA </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> IrDA </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> USB </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Standard GSM </th> <td> <ul> <li class="attr-value"> 900 </li> <li class="attr-value"> 850 </li> <li class="attr-value"> 1800 </li> <li class="attr-value"> 1900 </li> </ul> </td> </tr> <tr> <th> Standard UMTS </th> <td> <ul> <li class="attr-value"> 850 </li> <li class="attr-value"> 900 </li> <li class="attr-value"> 1700 </li> <li class="attr-value"> 1900 </li> <li class="attr-value"> 2100 </li> </ul> </td> </tr> <tr> <th> GPRS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> EDGE </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> HSPA </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> HSPA+ </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> HSDPA </th> <td> <ul> <li class="attr-value"> 42,00 Mbit/s </li> </ul> </td> </tr> <tr> <th> HSUPA </th> <td> <ul> <li class="attr-value"> 5,76 Mbit/s </li> </ul> </td> </tr> <tr> <th> HSCSD </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Profil A2DP </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> WAP </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> xHTML </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Multimedia</h3> </header> <table> <tbody> <tr> <th> Java </th> <td> <ul> <li class="attr-value"> ART </li> </ul> </td> </tr> <tr> <th> Gry </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Radio </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Dyktafon </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Tuner DVB-H </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Obsługa RSS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Funkcje biznesowe</h3> </header> <table> <tbody> <tr> <th> Klient e-mail </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Protokoły e-mail </th> <td> <ul> <li class="attr-value"> POP3 </li> <li class="attr-value"> IMAP4 </li> <li class="attr-value"> SMTP </li> </ul> </td> </tr> <tr> <th> Kalendarz </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Organizer </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Kalkulator </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Konwerter walut </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Budzik </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Tryb samolotowy </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> SyncML </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Parametry fizyczne</h3> </header> <table> <tbody> <tr> <th> Kolor </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Kolor:Srebrny.htm">Srebrny</a> </li> </ul> </td> </tr> <tr> <th> Wymienna obudowa </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Waga </th> <td> <ul> <li class="attr-value"> 144 g </li> </ul> </td> </tr> </tbody> </table>  <table> <tbody> <tr> <th> Popularne modele </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Popularne_modele:Huawei_P8.htm">Huawei P8</a> </li> </ul> </td> </tr> </tbody> </table> '),
(39953643, 772844, 'Apple', 'iPhone 6S 16GB Gwiezdna Szarość', 'Smartfony', 'Apple iPhone 6S 16GB Gwiezdna Szarość', ' <header class="section-header"> <h3>Dane podstawowe</h3> </header> <table> <tbody> <tr> <th> Producent </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/p:Apple.htm">Apple</a> </li> </ul> </td> </tr> <tr> <th> System operacyjny </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/System_operacyjny:Apple_iOS.htm">Apple iOS</a> </li> </ul> </td> </tr> <tr> <th> Typ budowy </th> <td> <ul> <li class="attr-value"> Dotykowy </li> </ul> </td> </tr> <tr> <th> Pamięć wewnętrzna </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Pamiec_wewnetrzna:16_GB.htm">16 GB</a> </li> </ul> </td> </tr> <tr> <th> Pamięć RAM </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Pamiec_RAM:2_GB.htm">2 GB</a> </li> </ul> </td> </tr> <tr> <th> Dual SIM </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Dual_SIM:Nie.htm">Nie</a> </li> </ul> </td> </tr> <tr> <th> Rodzaj karty SIM </th> <td> <ul> <li class="attr-value"> nanoSIM </li> </ul> </td> </tr> <tr> <th> Rodzaj karty pamięci </th> <td> <ul> <li class="attr-value"> Nie obsługuje </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Ekran</h3> </header> <table> <tbody> <tr> <th> Przekątna ekranu </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Przekatna_ekranu:4_7_cala.htm">4,7 cala</a> </li> </ul> </td> </tr> <tr> <th> Technologia wyświetlacza </th> <td> <ul> <li class="attr-value"> IPS TFT </li> </ul> </td> </tr> <tr> <th> Rozdzielczość ekranu </th> <td> <ul> <li class="attr-value"> 750x1334 </li> </ul> </td> </tr> <tr> <th> Ilość kolor&#243;w wyświetlacza </th> <td> <ul> <li class="attr-value"> 16M </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Aparat</h3> </header> <table> <tbody> <tr> <th> Wbudowany aparat cyfrowy </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Wbudowany_aparat_cyfrowy:12_Mpix.htm">12 Mpix</a> </li> </ul> </td> </tr> <tr> <th> Rozdzielczość aparatu </th> <td> <ul> <li class="attr-value"> 4608x2592 </li> </ul> </td> </tr> <tr> <th> Lampa błyskowa </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Zoom cyfrowy </th> <td> <ul> <li class="attr-value"> 3x </li> </ul> </td> </tr> <tr> <th> Przedni aparat </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Video </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Formaty video </th> <td> <ul> <li class="attr-value"> MPEG4 </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Łączność</h3> </header> <table> <tbody> <tr> <th> Moduł WiFi </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Hotspot WiFi </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Technologia LTE </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Technologia_LTE:Tak.htm">Tak</a> </li> </ul> </td> </tr> <tr> <th> NFC </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> GPS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> A-GPS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Bluetooth </th> <td> <ul> <li class="attr-value"> v4.2 </li> </ul> </td> </tr> <tr> <th> Miracast </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> DLNA </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> IrDA </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> USB </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Standard GSM </th> <td> <ul> <li class="attr-value"> 900 </li> <li class="attr-value"> 850 </li> <li class="attr-value"> 1800 </li> <li class="attr-value"> 1900 </li> </ul> </td> </tr> <tr> <th> Standard UMTS </th> <td> <ul> <li class="attr-value"> 850 </li> <li class="attr-value"> 900 </li> <li class="attr-value"> 1700 </li> <li class="attr-value"> 1900 </li> <li class="attr-value"> 2100 </li> </ul> </td> </tr> <tr> <th> GPRS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> EDGE </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> HSPA </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> HSPA+ </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> HSDPA </th> <td> <ul> <li class="attr-value"> 42,20 Mbit/s </li> </ul> </td> </tr> <tr> <th> HSUPA </th> <td> <ul> <li class="attr-value"> 5,76 Mbit/s </li> </ul> </td> </tr> <tr> <th> HSCSD </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Profil A2DP </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> WAP </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> xHTML </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Multimedia</h3> </header> <table> <tbody> <tr> <th> Java </th> <td> <ul> <li class="attr-value"> Nie obsługuje </li> </ul> </td> </tr> <tr> <th> Gry </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Radio </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Dyktafon </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Formaty audio </th> <td> <ul> <li class="attr-value"> AAC </li> <li class="attr-value"> WAV </li> <li class="attr-value"> AAX </li> <li class="attr-value"> AAX+ </li> <li class="attr-value"> AIFF </li> <li class="attr-value"> HE-AAC </li> </ul> </td> </tr> <tr> <th> Tuner DVB-H </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Wyjście TV </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Obsługa RSS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Funkcje biznesowe</h3> </header> <table> <tbody> <tr> <th> Klient e-mail </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Protokoły e-mail </th> <td> <ul> <li class="attr-value"> POP3 </li> <li class="attr-value"> IMAP4 </li> <li class="attr-value"> SMTP </li> </ul> </td> </tr> <tr> <th> Kalendarz </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Organizer </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Kalkulator </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Konwerter walut </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Budzik </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Tryb samolotowy </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> SyncML </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Zasilanie</h3> </header> <table> <tbody> <tr> <th> Ładowanie bezprzewodowe </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Technologia baterii </th> <td> <ul> <li class="attr-value"> Li-Po </li> </ul> </td> </tr> <tr> <th> Pojemność baterii </th> <td> <ul> <li class="attr-value"> 1715 mAh </li> </ul> </td> </tr> <tr> <th> Maksymalny czas czuwania </th> <td> <ul> <li class="attr-value"> 240 h </li> </ul> </td> </tr> <tr> <th> Maksymalny czas rozmowy </th> <td> <ul> <li class="attr-value"> 780 min </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Parametry fizyczne</h3> </header> <table> <tbody> <tr> <th> Kolor </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Kolor:Szary.htm">Szary</a> </li> </ul> </td> </tr> <tr> <th> Wymienna obudowa </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Wysokość </th> <td> <ul> <li class="attr-value"> 138,3 mm </li> </ul> </td> </tr> <tr> <th> Szerokość </th> <td> <ul> <li class="attr-value"> 67,1 mm </li> </ul> </td> </tr> <tr> <th> Grubość </th> <td> <ul> <li class="attr-value"> 7,10 mm </li> </ul> </td> </tr> <tr> <th> Waga </th> <td> <ul> <li class="attr-value"> 143 g </li> </ul> </td> </tr> <tr> <th> Czytnik linii papilarnych </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Procesor</h3> </header> <table> <tbody> <tr> <th> Model Procesora </th> <td> <ul> <li class="attr-value"> Apple A9 </li> </ul> </td> </tr> <tr> <th> Częstotliwość procesora </th> <td> <ul> <li class="attr-value"> 1,50 GHz </li> </ul> </td> </tr> <tr> <th> Ilość rdzeni procesora </th> <td> <ul> <li class="attr-value"> 4 </li> </ul> </td> </tr> </tbody> </table> ');

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `opinions`
--
ALTER TABLE `opinions`
  ADD CONSTRAINT `prdFK` FOREIGN KEY (`prd_id`) REFERENCES `products` (`prd_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
