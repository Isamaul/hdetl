<?php

class ETL_CmsUserForm extends Zend_Form
{

    protected $id = null;
    protected $user = null;

    public function __construct($id=null, $user = null){
        if (!empty($id)) {
            $this ->id = $id;
        }
        if (!empty($user)) {
            $this ->user = $user;
        }
        parent::__construct();
    }

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */


        $this -> addElement('text', 'usr_email', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Email',
            'class' => 'form-control'

        ));
        $this->usr_email ->addDecorator('Errors');
        $this -> usr_email ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));
        $this -> usr_email -> addValidator(new ETL_Valid_EmailExists('Model_DbTable_CmsUsers',$this->id));


        $this -> addElement('text', 'usr_name', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Imię',
            'class' => 'form-control'

        ));
        $this -> usr_name ->addDecorator('Errors');
        $this -> usr_name ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));

        $this -> addElement('text', 'usr_lastname', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Nazwisko',
            'class' => 'form-control'

        ));
        $this -> usr_lastname ->addDecorator('Errors');
        $this -> usr_lastname ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));

        if ($this->user != null && $this->user->usr_type == "ROOT") {

            $this->addElement('select','usr_type', array(
                'label' => 'Uprawnienia',
                'value' => 'blue',
                'class' => 'form-control',
                'multiOptions' => array(
                    'ROOT' => 'Admin',
                    'Worker' => 'User'
                )
            ));
        }

        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'value'    => 'Zapisz ',
            'label'    => 'Zapisz',
            'class'     => 'btn btn-primary  btn-fla'
        ));



        $this->setElementDecorators(array(
            'ViewHelper',
            'Errors',
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class'=> '')),
            array('Label', array('tag' => 'label', 'tagClass' => '' )),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
        ));

        $element = $this->getElement('submit');
        $element->removeDecorator('label');



    }


}

