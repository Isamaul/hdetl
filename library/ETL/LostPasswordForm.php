<?php

class ETL_LostPasswordForm extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */


        $this -> addElement('text', 'email', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Adres e-mail',
            'class' => 'form-control',
            'placeholder' => 'Email'
        ));
        $this->email ->addDecorator('Errors');
        $this -> email ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));








        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'value'    => 'Wyślij ',
            'label'    => 'Wyślij',
            'class'     => 'btn btn-primary  btn-flat'

        ));




        $this->setElementDecorators(array(
            'ViewHelper',
            'Errors',
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class'=> 'form-group has-feedback')),
//            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form'))
        ));

        $element = $this->getElement('submit');
        $element->removeDecorator('label');

    }


}

