<?php

class ETL_CmsLoginForm extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */


        $this -> addElement('text', 'usr_email', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Login',
            'class' => 'form-control',
            'placeholder' => 'Email'

        ));
        $this->usr_email ->addDecorator('Errors');
        $this -> usr_email ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));



        $this -> addElement('password', 'usr_password', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Hasło',
            'class' => 'form-control',
            'placeholder' => 'Hasło'

        ));
        $this->usr_password ->addDecorator('Errors');
        $this -> usr_password ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));


        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'value'    => 'Zaloguj ',
            'label'    => 'Zaloguj',
            'class'     => 'btn btn-primary  btn-flat'
        ));



        $this->setElementDecorators(array(
            'ViewHelper',
            'Errors',
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class'=> 'form-group has-feedback')),
//            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form'))
        ));

        $element = $this->getElement('submit');
        $element->removeDecorator('label');

//        $elements = $this->getElements();
//        foreach ($elements as &$elem) {
//            $elem->removeDecorator('label');
//        }
    }


}

