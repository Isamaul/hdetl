<?php

class ETL_FrontendController extends Zend_Controller_Action
{

    private $logged = false;
    protected $_user;
    public function init()
    {
        /* Initialize parent controller here */
        $config = $this->view->config = Zend_Registry::get('APPCONFIG');

        $optionsModel = new Model_DbTable_Options();
        $optionsList = $optionsModel->fetchAll();
        $options = array();
        foreach ($optionsList as $opt) {
            $key = $opt->opt_key;
            $options[$key] = $opt;
        }

        $this->view->menu= '';
        $this->view->options = $options;
        $this->view->headTitle($options['page_title']->getValue())
            ->setSeparator(' - ');


//        $this->view->headMeta()->appendName('description', $options['page_description']->getValue());
//        $this->view->headMeta()->appendName('keywords', $options['page_keywords']->getValue());

        $this::isLogged();

    }



    public function isCmsLogged() {
        $auth= Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $dane = $auth->getIdentity();
            $this->_user = $this->view->user = $dane;
            $this->view->logged = 1;
        } else {
            return $this-> _helper -> redirector ('index', 'auth', 'default');
        }



    }


    public function isLogged() {
        $auth= Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $dane = $auth->getIdentity();
            $this->_user = $this->view->user = $dane;
            $this->view->logged = 1;
            $this->logged=true;
        } else {
            $this->view->logged = 0;
            $this->logged=false;
        }


    }

    public function checkIfLogged() {
        if ($this->logged) return true;
        else return false;
    }



}

