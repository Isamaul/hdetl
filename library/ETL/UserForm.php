<?php

class ETL_UserForm extends Zend_Form
{

    protected $id = null;

    public function __construct($id=null){
        if (!empty($id)) {
            $this ->id = $id;
        }
        parent::__construct();
    }

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */


        $this -> addElement('text', 'usr_email', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Email',
            'class' => 'validate[required]'

        ));
        $this->usr_email ->addDecorator('Errors');
        $this -> usr_email ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));
        $this -> usr_email -> addValidator(new ETL_Valid_EmailExists('Model_DbTable_CmsUsers',$this->id));


        $this -> addElement('text', 'usr_name', array(
            'filters' => array('StringTrim','StripTags'),
            'label' => 'Imię',

        ));
        $this -> usr_name ->addDecorator('Errors');


        $this -> addElement('text', 'usr_lastname', array(
            'filters' => array('StringTrim','StripTags'),
            'label' => 'Nazwisko',

        ));
        $this -> usr_lastname ->addDecorator('Errors');

        $this -> addElement('text', 'usr_phone', array(
            'filters' => array('StringTrim','StripTags'),
            'label' => 'Telefon',
        ));

        $this -> addElement('text', 'usr_address', array(
            'filters' => array('StringTrim','StripTags'),
            'label' => 'Adres',
        ));

        $this -> addElement('text', 'usr_zip', array(
            'filters' => array('StringTrim','StripTags'),
            'label' => 'Kod pocztowy',
        ));

        $this -> addElement('text', 'usr_city', array(
            'filters' => array('StringTrim','StripTags'),
            'label' => 'Miasto',
        ));


        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'value'    => 'Zapisz ',
            'label'    => 'Zapisz',
            'class'     => 'submit'
        ));



        $this->setElementDecorators(array(
            'ViewHelper',
            'Errors',
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class'=> 'col_10 inputcont')),
            array('Label', array('tag' => 'div', 'tagClass' => 'col_2' )),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'formrow clearfix'))
        ));

        $element = $this->getElement('submit');
        $element->removeDecorator('label');

    }


}

