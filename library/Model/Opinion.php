<?php

class Model_Opinion extends Zend_Db_Table_Row_Abstract
{
    protected $_role;

    public function init()
    {

    }


    /**
     * @return bool|string
     * Zwraca datę wstawienia opinii
     */

   public function getDate() {
       $time = strtotime($this->date);
       return date('Y-m-d H:i',$time);
   }

    /**
     * @return string
     * Zwraca źródło opinii
     */

    public function getSource(){
        if (!empty($this->ceneoID)) {
            return 'Strona produktu na ceneo.pl, id opinii: ' . $this->ceneoID;
        } elseif (!empty($this->moreleID)) {
            return 'Strona produktu na morele.net, id opinii: ' . $this->moreleID;
        } else {
            return '';
        }
    }



}