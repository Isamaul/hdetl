<?php

class Model_Options extends Zend_Db_Table_Row_Abstract
{
    protected $_role;

    public function init()
    {

    }


    public function getValue()
    {
        $value = $this->opt_value;
        $isImage = $this->opt_image;

        if ($isImage>0) {
            $config = Zend_Registry::get('APPCONFIG');
            $url =$config->domainName.$value;
            return "<img class='listImage' src='$url'>";
        } else {
            return $value;
        }
    }



}