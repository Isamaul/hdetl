<?php

class Model_DbTable_Opinions extends Zend_Db_Table_Abstract
{

    protected $_name = 'opinions';
    protected $_rowClass = 'Model_Opinion';


    /**
     * @param $data
     * @return array
     * Zapisuje nowe rekordy porównując dane zapisane z pobranymi z serwisu ceneo.pl
     */

    public function processCeneo($data) {
        $ceneoIDs = array();

        $formattedData = array();

        foreach ($data as $d) {
            $ceneoIDs[] = $d['ceneoID'];
            $formattedData[$d['ceneoID']] = $d;
        }

        if (count($ceneoIDs)) {
            $select = $this->select()->where('ceneoID in (?)',$ceneoIDs);
            $existing =  $this->fetchAll($select);

            if (count($existing)) {
                foreach  ($existing as $elem) {
                    unset($formattedData[$elem['ceneoID']]);
                }
            }

            foreach ($formattedData as $d) {
                $this->insert($d);
            }

            return array('existingCeneo' => count($existing), 'insertedCeneo' => count($formattedData));
        } else {
            return array('existingCeneo' => 0, 'insertedCeneo' => 0);
        }



    }

    /**
     * @param $data
     * @return array
     * Zapisuje nowe rekordy porównując dane zapisane z pobranymi z serwisu morele.net
     */

    public function processMorele($data) {

        $moreleIDs = array();
        $formattedData = array();

        foreach ($data as $d) {
            $moreleIDs[] = $d['moreleID'];
            $formattedData[$d['moreleID']] = $d;
        }

        if (count($moreleIDs)) {
            $select = $this->select()->where('moreleID in (?)',$moreleIDs);
            $existing =  $this->fetchAll($select);

            if (count($existing)) {
                foreach  ($existing as $elem) {
                    unset($formattedData[$elem['moreleID']]);
                }
            }

            foreach ($formattedData as $d) {
                $this->insert($d);
            }

            return array('existingMorele' => count($existing), 'insertedMorele' => count($formattedData));
        } else {
            return array('existingMorele' => 0, 'insertedMorele' => 0);
        }



    }

    /**
     * @param $id
     * @return Zend_Db_Table_Rowset_Abstract
     * Pobiera opinie dla danego produktu
     */

    public function getForProduct($id) {
        return $this->fetchAll($this->select()->where('prd_id = ?', $id ));
    }

    public function truncateTable() {
        $this->getAdapter()->query('TRUNCATE TABLE '.$this->info(Zend_Db_Table::NAME));
    }

}

