<?php

class Model_DbTable_CmsUsers extends Zend_Db_Table_Abstract
{

    protected $_name = 'cms_users';


    public function mailExists($mail=null,$id=null) {
        $query = $this->select()->where('usr_email = ?',$mail);
        if (!empty($id)) {$query->where('usr_id <> ?',$id); }
        $result = $this->fetchAll($query);
        if ($result->count()>0) return true;
        else return false;
    }

    public function getByEmail($mail=null) {
        $select = $this->select()->where("usr_email = ?",$mail)->limit(1);
        $row = $this->fetchRow($select);
        if(!empty($row)) return $row;
        else return null;
    }

    public function getByHash($hash=null) {
        $select = $this->select()->where("usr_hash = ?",$hash)->limit(1);
        $row = $this->fetchRow($select);
        if(!empty($row)) return $row;
        else return null;
    }


}

