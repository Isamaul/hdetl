<?php

class Model_DbTable_Products extends Zend_Db_Table_Abstract
{

    protected $_name = 'products';


    /**
     * @param $id
     * @param $data
     * @throws Zend_Db_Table_Exception
     * Synchronizuje dane produktu
     */

    public function syncProduct($id,$data) {
        // check if exists
        $obj =  $this->find($id);
        $count = $obj->count();
        $data['prd_id'] = $id;
        if ($count == 0) {
            $this->insert($data);
        } else {
            $obj = $obj->current();
            $obj->setFromArray($data);
            $obj->save();
        }

    }

}

