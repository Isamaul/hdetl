var cms = {

    mainDomain: '',
    ajaxLoading: false,

    init:function() {

        this.initTables();
        this.initConfirm();
        this.initDatePickers();
        this.initModals();

        this.actionForm();
    },


    actionForm: function() {
        $('.loadForm').on('submit',function(e) {
            e.preventDefault();
            e.stopPropagation();
            var form = $(this);
            if (cms.ajaxLoading) return;
            cms.ajaxLoading = true;
            $('#overlay').show();
            $.ajax({
                url: '/index/extract',
                data: form.serialize(),
                method: "POST",
                success: function(data){
                    if (data.status == 'OK') {
                        console.log(data);
                        $('#scrapedData').text(data.pages);
                        $('#product').text(data.product.plain);
                        $('#type').text(data.product.type);
                        $('.modal.extract').modal('show');
                        $('.transformTrigger').removeClass('disabled');
                        $('.loadDataTrigger').addClass('disabled');
                        $('#overlay').hide();
                    } else {
                        alert('Napotkano błąd');
                        $('#overlay').hide();
                    }
                    cms.ajaxLoading = false;
                },
                error: function () { alert('Wystąpił nieoczekiwany błąd');  cms.ajaxLoading = false;  $('#overlay').hide(); }
            });
        });

        $('.transformTrigger').on('click',function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (cms.ajaxLoading || $(this).hasClass('disabled')) return;
            cms.ajaxLoading = true;
            $('#overlay').show();
            $.ajax({
                url: '/index/transform',
                method: "POST",
                success: function(data){
                    if (data.status == 'OK') {
                        console.log(data);
                        $('#ceneoCount').text(data.ceneo);
                        $('#moreleCount').text(data.morele);
                        $('.modal.transform').modal('show');


                        $('.transformTrigger').addClass('disabled');
                        $('.saveDataTrigger').removeClass('disabled');
                        $('#overlay').hide();
                    } else {
                        alert('Napotkano błąd');
                        $('#overlay').hide();
                    }
                    cms.ajaxLoading = false;
                },
                error: function () { alert('Wystąpił nieoczekiwany błąd');  cms.ajaxLoading = false;  $('#overlay').hide(); }
            });
        });



        $('.saveDataTrigger').on('click',function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (cms.ajaxLoading || $(this).hasClass('disabled')) return;
            cms.ajaxLoading = true;
            $('#overlay').show();
            $.ajax({
                url: '/index/sync',
                method: "POST",
                success: function(data){
                    if (data.status == 'OK') {
                        console.log(data);
                        $('#newCount').text(data.insertedCeneo + data.insertedMorele);
                        $('#untouchedCount').text(data.existingCeneo + data.existingMorele);
                        $('#productLink').attr('href',data.url);
                        $('.modal.load').modal('show');

                        $('.saveDataTrigger').addClass('disabled');
                        $('#overlay').hide();
                    } else {
                        alert('Napotkano błąd');
                        $('#overlay').hide();
                    }
                    cms.ajaxLoading = false;
                },
                error: function () { alert('Wystąpił nieoczekiwany błąd');  cms.ajaxLoading = false;  $('#overlay').hide(); }
            });
        });

        $('.etlTrigger').on('click',function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (cms.ajaxLoading || $(this).hasClass('disabled')) return;
            cms.ajaxLoading = true;
            $('#overlay').show();
            $.ajax({
                url: '/index/etl',
                data: { productid: $('[name=productid]').val()},
                method: "POST",
                success: function(data){
                    if (data.status == 'OK') {
                        console.log(data);
                        $('#newCount').text(data.insertedCeneo + data.insertedMorele);
                        $('#untouchedCount').text(data.existingCeneo + data.existingMorele);
                        $('#productLink').attr('href',data.url);
                        $('.modal.load').modal('show');

                        $('.saveDataTrigger').addClass('disabled');
                        $('#overlay').hide();
                    } else {
                        alert('Napotkano błąd');
                        $('#overlay').hide();
                    }
                    cms.ajaxLoading = false;
                },
                error: function () { alert('Wystąpił nieoczekiwany błąd');  cms.ajaxLoading = false;  $('#overlay').hide(); }
            });
        });


    },



    initModals: function() {
        $('.ajaxModal').on('click',function(e){
            e.preventDefault();
            $('.modal').remove();
            if (!cms.ajaxLoading) {
                var href = $(this).attr('href');
                cms.ajaxLoading = true;
                $.ajax(href,{
                    method: "POST",
                    success: function(data){
                        cms.ajaxLoading = false;
                        $('body').append($(data));
                        $('.modal').modal('show');
                    }
                })
            }
        });
    },

    initConfirm: function() {
        $('.confirmAction').on('click',function(e){
            if (!confirm('Czy jesteś pewien?')) e.preventDefault();
        });

    },

    initDatePickers: function() {
        $('.datepicker').datepicker({
                orientation: 'top',
                autoclose: true,
                format: 'yyyy-mm-dd'
            });

    },

    initTables: function() {
        $(".customTable").DataTable({
            "paging": false,
            info: false
        });
    }




};